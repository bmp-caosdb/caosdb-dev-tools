# Formatters

* XML: `xml_pp -s indented -e UTF-8 -i <FILE>`
* JAVA: `mvn fmt:format`
* PYTHON: `autopep8 -aa` and `docformatter`
* JAVASCRIPT: `js-beautify`

