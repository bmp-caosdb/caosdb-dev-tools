#!/bin/bash
#
# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2018 Research Group Biomedical Physics,
# Max-Planck-Institute for Dynamics and Self-Organization Göttingen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header
#
# Add a license header to all files in DIR and all subdirectories and print all
# remaining files without a header.
# usage: ./add-license-header DIR

# Note for adding the encoding line:
# I used the following one-liner:
# for f in $(find .. -name "*.py" -exec grep -L -E '^[ \t\f]*#.*?coding[:=][ \t]*([-_.a-zA-Z0-9]+)' {} \; ); do LINE=$(head -n 1 "$f" | grep -c '^#!') ; LINE=$((LINE+1)); echo $LINE $f; sed -i "${LINE}i\# encoding: utf-8" "$f" ; done


DIR="$1" #  CaosDB root directory
EXCLUDE='! -path */caosdb-server/caosdb-webui/* ! -path */.git/*'
# Fancy header line.  Allows to replace the license section by a different one
# in the future.
MAGIC_STRING=$(head -n 1 license-header)
IGNORE_FILE=() # The ignored basenames will be Bash-expanded
IGNORE_FILE+=(".gitignore~")
IGNORE_FILE+=(".gitignore")
IGNORE_FILE+=(".git")
IGNORE_FILE+=(".gitmodules")
IGNORE_FILE+=("LICENSE*")
IGNORE_FILE+=("README*")
IGNORE_FILE+=("VERSIONING*")
IGNORE_FILE+=("WELCOME*")
IGNORE_FILE+=("readme*")
IGNORE_FILE+=("*.properties")
IGNORE_FILE+=("lorem_ipsum.txt")
IGNORE_FILE+=("hello?(_)world.txt")
IGNORE_FILE+=("cache.ccf")
IGNORE_FILE+=("license-header*")
IGNORE_FILE+=("custom.magic")
IGNORE_FILE+=("image_source.txt")
# IGNORE_FILE+=("annotation.xsl.js")
# IGNORE_FILE+=("entity.xsl.js")
IGNORE_PATH=() # Matched against the complete path
IGNORE_PATH+=("*/.tox/*")
IGNORE_PATH+=("*.pyc")

# We want extended pattern matching options
shopt -s extglob

# Regular expressions for start and end of header
START_RE='\*\* header v'
END_RE='\*\* end header'
# Globals are the best way to pass around arrays.
declare -a files

# Puts all files which don't have the license header (in the correct version)
# into the "files" array.
#
# param $1: a directory
# param $2: (optional) a case insensitive file name pattern
function find_files_without_correct_header(){
    local dir="$1"
    local iname="${2:-*}"
    local file_array=()
    while IFS=  read -r -d $'\0' file; do
        file_array+=("$file")
    done < <(find "$dir" -type f -iname "$iname" $EXCLUDE -print0)

    files=()
    for file in "${file_array[@]}"; do
        grep -Fe "$MAGIC_STRING" -q "$file" \
            || files+=("$file")
    done
    echo "${#files[*]} files without the correct MAGIC line"
}

# Deletes an old header from a file
#
# param $1: the file
function delete_old_header(){
    local FILE="$1"
    # first test if there is an old header at all
    local HEADER_LINES=$(sed -e "/${START_RE}/,/${END_RE}/p;d" "$FILE" | wc -l)
    [[ "$HEADER_LINES" -ge 2 ]] || return 0
    # It's sed magic!!
    # Similar to https://unix.stackexchange.com/a/249433
    local EXP="/${END_RE}/,+1d;/${START_RE}/!N;/${START_RE}/,/${END_RE}/!P;D"
    sed -e"${EXP}" -i "$FILE"
}

# Print 1 iff the file has a shebang line, 0 otherwise.
# param $1: the file
function shebang_count(){
    head -n 1 "$1" | grep -c '^#!'
}

# Print 1 iff the file has a python-style encoding line in the first two lines,
# 0 otherwise.
# param $1: the file
function encoding_count(){
    # Officially, as per PEP-0263:
    # ^[ \t\f]*#.*?coding[:=][ \t]*([-_.a-zA-Z0-9]+)
    # Old RE:  '^#.*cod.*'
    head -n 2 "$1" | grep -c -E '^[ \t\f]*#.*?coding[:=][ \t]*([-_.a-zA-Z0-9]+)'
}

# Print 1 iff the file has a html doctype declaration, 0 otherwise.
# param $1: the file
function doctype_count(){
    head -n 1 "$1" | grep -c -i '^<\!doctype'
}

# Print 1 iff the file has a xml prolog, 0 otherwise.
# param $1: the file
function xml_prolog_count(){
    head -n 1 "$1" | grep -c '^<?xml'
}

# Tests if a file is in the ignore list
# param $1: the file name
function test_ignore(){
    local base=$(basename "$1")
    for ig in "${IGNORE_FILE[@]}"; do
        [[ $base == $ig ]] && return 0
    done
    for ig in "${IGNORE_PATH[@]}"; do
        [[ $base == *$ig ]] && return 0
    done
    return 1
}

# Add the content of the 'header_file' to the head of the 'file' with an 'offset'
# param $1: the file
# param $2: the header_file
# param $3: the offset, in lines.  0 if omitted.
function add_header_at_line() {
    local file="$1"
    local header_file="$2"
    local offset="${3:-0}" # Default is 0.

    # echo "file: (${offset}) ${file}"
    # return 0

    # no offset
    if [[ "$offset" -eq "0" ]]; then
        echo "$(cat $header_file $file)" > "$file"
    else
        sed -i "${offset}r${header_file}" "$file"
    fi
    echo "Added license header to $file"
}

##################################################################
############ Specialized header functions ########################

# Dummy function for a function that still needs to be implemented
function add_header_not_implemented() {
    echo "Not implemented yet."
    echo -e "Args: $*"
}

# Add the header to a C-like file
# param $1: File to be fixed
# Types: c, c++, css, g4, java, js, sql
function add_header_c() {
    local f="$1"
    delete_old_header "$f"
    add_header_at_line "$f" license-header-c.txt
}

# Add the header to a hash-commenting file
# param $1: File to be fixed
# Adds the #-commented license text after the shebang (if it exists).
# Used for: sh, Makefile, yml
function add_header_hash() {
# All python files seem to belong to us, only logger.py (see
# unclarified-files.txt) is a bit unclear.
    local f="$1"
    local line=$(shebang_count "$f")
    delete_old_header "$f"
    add_header_at_line "$f" license-header-hash.txt "$line"
}

# Add the header to a TeX-like file
# param $1: File to be fixed
# Also: tex
function add_header_percent() {
    local f="$1"
    delete_old_header "$f"
    add_header_at_line "$f" license-header-percent.txt
}

# Add the header to a Python-like file
# param $1: File to be fixed
# Adds the #-commented license text after the shebang and the encoding line (if
# they exist).
function add_header_python() {
# All python files seem to belong to us, only logger.py (see
# unclarified-files.txt) is a bit unclear.
    local f="$1"
    delete_old_header "$f"
    local line=$(( $(shebang_count "$f") + $(encoding_count "$f") ))
    add_header_at_line "$f" license-header-hash.txt "$line"
}

# Add the header to an HTML-like file
# param $1: File to be fixed
function add_header_html() {
    # all *.html files seem to belong to us (check unclarified-files.txt)
    local f="$1"
    delete_old_header "$f"
    add_header_at_line "$f" license-header-xml.txt "$(doctype_count $f)"
}

# Add the header to an XML-like file
# param $1: File to be fixed
function add_header_xml() {
    local f="$1"
    delete_old_header "$f"
    add_header_at_line "$f" license-header-xml.txt "$(xml_prolog_count $f)"
}

# Add the header to a PlatUML-like file
# param $1: File to be fixed
function add_header_plant() {
    local f="$1"
    delete_old_header "$f"
    add_header_at_line "$f" license-header-plant.txt 1
}

################################
######## Main script ###########
################################

# 1. Iterate over ALL files
# 2. Check if the magic words exists already, then continue with next
# 3. Else, check for file type.  Use `file`, which does not depend on extension.
# 4. Print all unclassified files
# while IFS= read -d $'\0' -r f ; do
find_files_without_correct_header "$DIR" "*"
for f in "${files[@]}"; do
    if test_ignore "$f" ; then
        echo "ignored: $f"
        continue
    fi
    echo "processing $f"
    mime=$(file -m ./custom.magic:/usr/share/misc/magic.mgc -b --mime-type "$f")
    base=$(basename "$f")
    ext=".${f##*.}"
    #################
    # Test MIME types
    case "$mime" in
        "application/x-executable" | "application/x-archive" ) continue ;;
        "application/zip" | "application/pdf" ) continue ;;
        "inode/x-empty" ) continue ;; # Ignore empty files
        "image/"* ) continue ;; # Ignore all images
        "text/x-c" | "text/x-c++") add_header_c "$f" ;;
        "text/html") add_header_html "$f" ;;
        "text/x-java") add_header_c "$f" ;;
        "text/x-makefile") add_header_hash "$f" ;;
        "text/x-python") add_header_python "$f" ;;
        "text/x-shellscript") add_header_hash "$f" ;;
        "text/x-tex") add_header_percent "$f" ;;
        "text/xml") add_header_xml "$f" ;;
        *)
            human=$(file -m ./custom.magic:/usr/share/misc/magic.mgc -b "$f")
            false ;; # MIME type not reconized
    esac || case "${human}" in
        #######################################
        # Test human-readable type descriptions
        "exported SGML document,"*) add_header_html "$f" ;; #echo "XML: $f"
        *) false ;;
    esac || case "$ext" in
        ######################
        # Test file extensions
        ".css" )  add_header_c "$f" ;;
        ".template" ) add_header_hash "$f" ;;
        ".ini" | ".cfg" | ".conf" ) true ;; # add_header_not_implemented "$f" ;;
        ".g4" ) add_header_c "$f" ;;
        ".java" ) add_header_c "$f" ;;
        ".js" ) add_header_c "$f" ;;
        ".org" ) true ;; # add_header_not_implemented "$f" ;;
        ".pu" ) add_header_plant "$f" ;;
        ".py" ) add_header_python "$f" ;;
        ".sql" ) add_header_c "$f" ;;
        ".yml" ) add_header_hash "$f" ;;
        ".xml" | ".xsl" ) add_header_xml "$f" ;;
        * ) false ;;
    esac || case "$base" in
        # Test special file names
        "makefile" | "Makefile" ) add_header_hash "$f" ;;
        "patch_header.sh" ) add_header_hash "$f" ;;
        * ) false ;;
        # No test matches!
    esac || echo "Unknown type '${mime} / ${human}': ${f}"

done # < <(find "$DIR" -type f $EXCLUDE -print0)

