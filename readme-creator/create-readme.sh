#!/bin/bash
#
# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2018 Research Group Biomedical Physics,
# Max-Planck-Institute for Dynamics and Self-Organization Göttingen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header
#

# create-readme.sh
# create a README.md and all other important files (LICENSE.md) in the subrepo
#
# useage: configure the config file and run
# `./create-readme config repo-dir repo-name`


CONFIG_FILE=$1
DIR=$2
REPO_NAME=$3

source $CONFIG_FILE

README=$DIR/README.md
LICENSE=$DIR/LICENSE.md
SETUP=$DIR/README_SETUP.md

if [ -f $README ]; then
    echo "$README does exist already. Please delete it or rename it to $SETUP and start this again."
    exit 1
fi


cp $LICENSE_TEMPLATE $LICENSE
cp $README_TEMPLATE $README

sed -i "/<WELCOME>/ {
    r $WELCOME_TEMPLATE
    d
}" $README
sed -i "s/<REPO_NAME>/$REPO_NAME/g" $README

ESCAPED_CAOSDB_ROOT_REPO=$(echo $CAOSDB_ROOT_REPO | sed -e 's/[\/&]/\\&/g')
sed -i "s/<CAOSDB_ROOT_REPO>/$ESCAPED_CAOSDB_ROOT_REPO/g" $README

if [ -f $SETUP ]; then
    sed -i "/<README_SETUP>/r $README_SETUP_TEMPLATE" $README
fi
sed -i '/<README_SETUP>/d' $README

