#!/usr/bin/env python3
"""Creates archives of a git repository and submodules.
"""

import argparse
import git
import os

def parse():
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('--dir', '-d', type=str,
                        default=".",
                        help='Directory to archive.')
    parser.add_argument('--tree', '-t',
                        type=str, default=None,
                        help='Tree or commit to archive.')
    parser.add_argument('--recursive', '-r', action='store_true',
                        help='Whether to also archive submodules.')

    args = parser.parse_args()
    return args

def tree_test(repo, treeish=None):
    """Tests if a repo contains a tree-ish.

    Returns True or False.  If 'treeish' is not True-ish, returns True."""
    if not treeish:
        return True
    if treeish in [x.name for x in repo.refs]:
        return True
    if any([c.hexsha.startswith(treeish) for c in repo.iter_commits()]):
        return True
    # OK, checked all possibilities, no candidate found.
    print(treeish)
    return False


def archive(repo, name, tree, recursive=False):
    """Archive a git repository."""
    arch_name = "{name}.tar.gz".format(name=name)
    # Test if the tree-ish exists at all
    if not tree_test(repo, treeish=tree):
        print("Are you certain there is '{tree}' in {repo}?".format(
            tree=tree,
            repo=repo.working_dir
        ))
        print("Omitting this repository (and children).")
        return
    with open(arch_name, 'wb') as ostream:
        repo.archive(ostream, treeish=tree, format="tar.gz")
    if recursive:
        # Find submodules and run the function there.
        for sub_mod in repo.submodules:
            sub_name = "_".join([name, sub_mod.name])
            sub_repo = sub_mod.module()
            archive(sub_repo, sub_name, tree, recursive=recursive)


def main():
    args = parse()
    repo = git.Repo(args.dir)
    name = repo.working_dir.split(os.path.sep)[-1]
    archive(repo, name, args.tree, recursive=args.recursive)

if __name__ == "__main__":
    main()
